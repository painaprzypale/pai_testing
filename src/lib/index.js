const request = require('request-promise-native');
const sleep = ms => new Promise(res => setTimeout(res, ms));

const createRandomString = (len) => {
    return Math.random().toString(36).substring(2, len / 2 + 2) + Math.random().toString(36).substring(2, len / 2 + 2);
};

const genRandomValue = (type) => {
    switch (type) {
        case 'Int':
            return Math.floor(Math.random() * 5000);
        case 'Boolean':
            return false; // it is random, I did roll dice
        default:
            return createRandomString(parseInt(type.split('_').pop()));
    }
};

const genRandomObj = (fields) => {
    let keys = Object.keys(fields);
    let result = {};

    for (let i = 0; i < keys.length; ++i) {
        result[keys[i]] = genRandomValue(fields[keys[i]]);
    }

    return result;
};

const urlBase = 'http://127.0.0.1:9000/v1';

const createFakeRecord = async function (obj, endpoint) {
    const options = {
        uri: `${urlBase}/${endpoint}`,
        method: 'POST',
        body: obj,
        json: true
    };

    const start = Date.now();
    const response = await request(options);
    return {
        response,
        elapsed: Date.now() - start
    };
};

const singleGet = async function (endpoint) {
    const options = {
        uri: `${urlBase}/${endpoint}`,
        method: 'GET',
        json: true
    };

    const start = Date.now();
    const response = await request(options);
    return {
        response,
        elapsed: Date.now() - start
    };
};


const testWrite = async function (fields, endpoint, tests=100, chunks=1) {
    var executionData = [];
    for (let i = 0; i < chunks; ++i) {
        let globalStart = Date.now();
        let data = [];

        for (let i = 0; i < tests; ++i) {
            data.push(createFakeRecord(genRandomObj(fields), endpoint));
        }
        let result = await Promise.all(data);
        let filtered = result.map(e => e.elapsed);
        let avgResponse = filtered.reduce((a,b) => a+b, 0)/filtered.length;
        let took = Date.now() - globalStart
        console.log(`Write test result:`);
        console.log(`Avg. response time: ${avgResponse} ms.`);
        console.log(`Test took: ${took}  ms.`);
        executionData.push({ took, avgResponse });
        await sleep(100)
    }

    console.log(`Summary:`)
    console.log(`Avg. response: ${executionData.map(e => e.avgResponse).reduce((a,b) => a+b, 0)/executionData.length}`);
    console.log(`Avg. chunk time: ${executionData.map(e => e.took).reduce((a,b) => a+b, 0)/executionData.length}`)
};

const testRead = async function (endpoint, tests=100, chunks=1) {
    var executionData = [];
    for (let i = 0; i < chunks; ++i) {
        let globalStart = Date.now();
        let data = [];

        for (let i = 0; i < tests; ++i) {
            data.push(singleGet(endpoint));
        }
        let result = await Promise.all(data);
        let filtered = result.map(e => e.elapsed);
        let avgResponse = filtered.reduce((a,b) => a+b, 0)/filtered.length;
        let took = Date.now() - globalStart
        console.log(`Read test result:`);
        console.log(`Avg. response time: ${avgResponse} ms.`);
        console.log(`Test took: ${took}  ms.`);
        executionData.push({ took, avgResponse });
        await sleep(100)
    }

    console.log(`Summary:`)
    console.log(`Avg. response: ${executionData.map(e => e.avgResponse).reduce((a,b) => a+b, 0)/executionData.length}`);
    console.log(`Avg. chunk time: ${executionData.map(e => e.took).reduce((a,b) => a+b, 0)/executionData.length}`)
}

module.exports = {
    genRandomObj,
    testWrite,
    testRead
};
