module.exports = {
    'original_department_id': 'Int',
    'receiver_address': 'String_20',
    'weight': 'Int',
    'last_modification_date': 'Int',
    'sync': 'Boolean'
};
