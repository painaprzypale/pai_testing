const lib = require('./lib');
const commandLineArgs = require('command-line-args');
const optionDefinitions = [
    { name: 'model', alias: 'm', type: String},
    { name: 'concurrent', alias: 'c', type: Number },
    { name: 'loops', alias: 'l', type: Number }
]
const { model, concurrent, loops } = commandLineArgs(optionDefinitions);

lib.testRead(model, concurrent, loops).catch(console.error);
