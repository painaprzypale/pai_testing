# PAI Backend testing framework

## Requirements

Node v8+
npm
yarn

## Installation

`yarn`

## Usage

`node src/test_read.js -m modelName`
